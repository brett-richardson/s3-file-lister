# S3 File Lister

## Getting Started

* Install docker
  https://docs.docker.com/docker-for-mac/install/

* Run docker and wait for the daemon to be ready

* Update the `docker-compose.yml` file with your AWS credentials and the
  bucket name you wish to target.

* Run the `docker-compose up` command.

* Navigate to http://localhost:8080 and bask in the brilliance that is
  Simple Storage in the cloud.
