const S3 = require('aws-sdk/clients/s3')
const REGION = 'eu-west-2'

class BucketLister {
  constructor(options) {
    this.bucket = options.bucket

    this.client = new S3({
      region: REGION,
      awsAccessKeyId: options.awsAccessKeyId,
      awsSecretAccessKey: options.awsSecretAccessKey,
    })
  }

  files() {
    return this.client.listObjects({
      Bucket: this.bucket
    }).promise().then(
      ({ Contents }) => Contents.map(this.processFile.bind(this))
    )
  }

  // private

  processFile(fileJSON) {
    const url = this.client.getSignedUrl('getObject', { Bucket: this.bucket, Key: fileJSON.Key })
    return Object.assign({ DownloadUrl: url }, fileJSON)
  }
}

module.exports = BucketLister
