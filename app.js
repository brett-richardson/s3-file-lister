const BucketLister = require("./src/bucket_lister")
const app = require('express')()

const HTTP_OK = 200
const HTTP_ERROR = 500

const {
  AWS_ACCESS_KEY_ID,
  AWS_SECRET_ACCESS_KEY,
  AWS_S3_BUCKET
} = process.env

const lister = new BucketLister({
  bucket: AWS_S3_BUCKET,
  accessKeyId: AWS_ACCESS_KEY_ID,
  secretAccessKey: AWS_SECRET_ACCESS_KEY,
})

app.get('/', (request, responder) => {
  lister.files().then(files => {
    responder.status(HTTP_OK).json({ files })
  }).catch(error => {
    console.error(error)
    response.status(HTTP_ERROR).json({ error: 'An error occurred' })
  })
})

module.exports = app
