const app = require('../../app.js')

describe('GET /', () => {
  it('returns a list of files', (done) => {
    chai.request(app).get('/').end((error, response) => {
      expect(error).to.equal(null)

      const firstFile = response.body.files[0]
      expect(firstFile.DownloadUrl).to.contain("amazonaws.com")
      expect(firstFile.Size).to.be.a('number')
      done()
    })
  })
})
