const BucketLister = require('../../src/bucket_lister')
const { AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_S3_BUCKET } = process.env

describe("BucketLister", () => {
  it("lists buckets", function () {
    const lister = new BucketLister({
      accessKey: AWS_ACCESS_KEY_ID,
      secret: AWS_SECRET_ACCESS_KEY,
      bucket: AWS_S3_BUCKET,
    })

    return lister.files().then(fileList => {
      expect(fileList[0].Key).to.be.a('string')
    })
  })
})
