require('dotenv').config()

const app = require("./app")
const port = process.env.PORT || 8080

console.log(`Starting server on ${port}`)
app.listen(port)
