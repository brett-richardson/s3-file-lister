FROM node:10-alpine

COPY . .

RUN yarn

CMD node index.js
